\documentclass{article}
\usepackage[utf8]{inputenc}

\title{Density of Discriminants of Orders of \(S_4\)-Quartic Fields}
\author{Jad Elkhaleq Ghalayini}
\date{November 28 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}
\usepackage{braket}

\usepackage[margin=1in]{geometry}

\newtheorem*{theorem}{Theorem}
\newtheorem*{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}
\newcommand{\prt}[2]{{\frac{\partial #1}{\partial #2}}}
\newcommand{\acts}[0]{\curvearrowright}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\Span}{span}
\DeclareMathOperator{\Id}{id}
\DeclareMathOperator{\Image}{Image}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Disc}{Disc}
\DeclareMathOperator{\smdisc}{disc}
\DeclareMathOperator{\Pos}{Pos}
\DeclareMathOperator{\Gal}{Gal}
\DeclareMathOperator{\SL}{SL}
\DeclareMathOperator{\Cl}{Cl}
\DeclareMathOperator{\Vol}{Vol}

\newcommand{\TODO}[1]{\text{\textbf{TODO: }{#1}}}
\newcommand{\TODOD}[1]{\begin{center}\large{\TODO{#1}}\end{center}}

\begin{document}

\maketitle

The goal of today is to prove the following limit from Chapter 5 of Bhargava's bachelor's thesis:
\[\lim_{x \to \infty}\frac{M(0 ; x)}{x} = \frac{\zeta(2)^3\zeta(3)}{48\zeta(5)}\]
where \(M(0; x)\) is the number of quartic orders of \(S_4\)-quartic fields with \(|\Disc| < x\). So, let's begin.

\section*{Introduction}

Recall that there is a bijection between elements \((A, B) \in V_\ints\) (where \(A, B\) are binary cubic forms) and isomorphism classes of \((Q, R)\) where \(Q\) is a quartic ring and \(R\) is its cubic resolvent ring. We have
\[2 \cdot (A, B) = ((a_{ij}), (b_{ij}))\]
where \((a_{ij}), (b_{ij})\) are 3-by-3 symmetric matrices. We can hence define an action on these by elements of
\[G_\reals = \GL{2}{\reals} \times \SL_3(\reals)\]
\begin{definition}
\((A, B) \in V_{\ints}\) are \textbf{absolutely irreducible} when
\begin{enumerate}[label=(\alph*)]

  \item \(A, B\) do not possess any common zeros (as binary cubic forms)

  \item The cubic form \(f(x, y) = \det(Ax + By)\) is irreducible over \(\rationals\).

\end{enumerate}
\end{definition}
We then have
\begin{theorem}[Theorem 7]
Let \(N(V_\ints; x)\) be the number of equivalence classes of pairs \((A, B) \in V_\ints\) such that \(|\Disc(A, B)| < X\). Then
\[\lim_{x \to \infty}\frac{N(V_\ints; x)}{x} = \frac{\zeta(2)^2\zeta(3)}{48}\]
\end{theorem}
So, we're going to prove this, we're going to divide by \(\zeta(5)\zeta(6)\), and then we're going to get rid of the \(\zeta(6)\) to prove the desired result. Let's get to it.

\section*{Reduction Theory}

Let \(\mc{F}\) be the fundamental domain for the action of \(G_\ints\) on \(G_\reals\). We have
\(\mc{F} \subset NA'K\Lambda\)
where
\[K = \{\text{orthogonal transformations in} \ G_\reals\}\]
\[A' = \left\{a(s_1, s_2, s_3) : a(s_1, s_2, s_3) = \begin{pmatrix} s_1^{-1} & 0 \\ 0 & s_2^{-1} \end{pmatrix} \cdot \begin{pmatrix}
  s_2^{-1}s_3^{-1} && \\ & s_2s_3^{-1} & \\ && s_2s_3^2
\end{pmatrix}\right\}\]
\[N' = \left\{n(u_1, u_2, u_3, u_4) : n(*) = \left(
  \begin{pmatrix} 1 & \\ u_1 & 1 \end{pmatrix},
  \begin{pmatrix} 1 && \\ u_2 & 1 & \\ u_3 & u_4 & 1 \end{pmatrix}
\right)\right\}\]
\[\Lambda = \{\lambda : \lambda > 0\}\]
Now, for any \(v \in V_\reals\), \(\mc{F}_v\) is the union of the fundamental domains for \(G_\ints \acts V_\reals\). (\(n = 24\))

Therefore, counting \(N(V_\ints, x)\) reduces to counting the number of lattice points in \(\mc{F}_v\) for a suitable \(v\). Davenport showed that this is very hard for a single \(v \in V_\reals\). Instead, we average a box, \(H\).

\section*{Averaging Over Fundamental Domains}
We define a box
\[H = \{(A, B) \in V_\reals: |a_{ij}|, |b_{ij}| \leq 10, |\Disc(A, B)| \geq 1\}\]
We then have
\[N(V_\ints; X) = \frac{
  \int_{v \in V_\reals}\Phi_H(v) \cdot \#\{x \in \mc{F}_v \cap V_\ints : x \ \text{is absolutely irreducible} , |\Disc(x)| < X\}|\Disc(v)|^{-1}dv
}{
  24\Vol(H)
}\]
It is important to count \(N(S; x)\) where \(S\) is a \(G_\ints\)-invariant subset. Given that \(|\Disc(v)|^{-1}dv\) is unique up to scaling, we can express \(N(S; x)\) as the following
\[N(S; X) = \frac{c'}{24\Vol(H)}\int_{g \in \mc{F}^{-1}}
  \sum_{\substack{
    x \in V \cap S \\
    x \ \text{is abs. irr.} \\
    |\Disc(x)| < X
  }}\Phi_H(gx)dg
\]
Given that \(\mc{F}^{-1} \subset K{A'}^{-1}N'\Lambda\), let \(dg\) be the Harr measure of \(G_\reals\). We then have
\[N(S; X) < \int_{g \in K{A'}^{-1}N'\Lambda}\sum_{\substack{
  x \in V \cap S \\
  x \ \text{is abs. irr.} \\
  |\Disc(x)| < X
}}\Phi_H(Kna^{-1}\lambda x)s_1^{-2}s_2^{-6}s_3^{-6}d^x\lambda d^xtdndk\]

\end{document}
