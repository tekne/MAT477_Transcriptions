\documentclass{article}
\usepackage[utf8]{inputenc}

\title{Counting in the cusp}
\author{Jad Elkhaleq Ghalayini}
\date{November 21 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}
\usepackage{braket}

\usepackage[margin=1in]{geometry}

\newtheorem*{theorem}{Theorem}
\newtheorem*{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}
\newcommand{\prt}[2]{{\frac{\partial #1}{\partial #2}}}

\newtheorem{definition}{Definition}
\newtheorem*{proposition}{Proposition}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\Span}{span}
\DeclareMathOperator{\Id}{id}
\DeclareMathOperator{\Image}{Image}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Disc}{Disc}
\DeclareMathOperator{\smdisc}{disc}
\DeclareMathOperator{\Pos}{Pos}
\DeclareMathOperator{\Gal}{Gal}
\DeclareMathOperator{\SL}{SL}

\newcommand{\TODO}[1]{\text{\textbf{TODO: }{#1}}}
\newcommand{\TODOD}[1]{\begin{center}\large{\TODO{#1}}\end{center}}

\begin{document}

\maketitle

\begin{center}
Based on a presentation by Qingchong Zhu
\end{center}

\setlength{\parindent}{0cm}

This lecture is about counting in the cusp. We begin by stating the main result we will attempt to prove:
\[\lim_{x \to \infty}\frac{\sum_{0 < \Disc(O) < X}|I_3(O)|}{\sum_{0 < \Disc(O) < X}1} = \frac{\zeta(2)}{\zeta(3)}\]
Before we go into how to prove this, we begin with some background definitions
\begin{definition}
We define the 4-dimensional \(\ints\)-module of binary cubic forms
\[V_\ints = \{ax^3 + bx^2y + cxy^2 + dy^3 : a, b, c, d \in \ints\}\]
\end{definition}
\begin{definition}
We define the submodule
\(V^*_\ints = \{ax^3 + 3bx^2y + 3cxy^2 + dy^3: a, b, c, d \in \ints\} \subset V_\ints\)
\end{definition}
\begin{definition}
\(\smdisc(f) = -\frac{1}{27}\Disc(f) = -3b^2c^2 + 4ac^3 + 4b^3d + a^2d^2 - 6abcd\)
\end{definition}

We then recall the theorem
\begin{theorem}
There exists a natural bijection between the nondegenerate \(\SL_2(\ints)\)-orbits on \(V^*_{\ints}\) and the set of equivalence classes of tuples \((O, I, \delta)\).
\end{theorem}

We can use this to define a group as follows:
\begin{definition}
We define a composition law, for fixed \(O\),
\[(O, I, \delta) \circ (O, I', \delta') = (O, II', \delta\delta')\]
\end{definition}
\begin{definition}
A binary cubic form \(f\), or its corresponding triple \((O, I, \delta)\), is \textbf{projective} if \(I\) is projective as an \(O\)-module, i.e. \(I\) is invertible and \(I^3 = (\delta)\).
\end{definition}
\begin{definition}
By fixing \(O\), we can define a group under the above composition law, denoted
\[H(O) = \{(O, I, \delta) : I \text{ is projective}\}\]
\end{definition}
\begin{definition}
To every binary quadratic form \(f(x, y) = ax^3 + 3bx^2y + 3cxy^2 + dy^3\), we can assign the covariant Hessian
\[Q_f(x, y) = Ax^2 + Bxy + Cy^2\]
where \(A = b^2 - ac, B = ad - bc, C = c^2 - bd\).
\end{definition}

We can then show
\begin{proposition}
\(f \in V^*_\ints\) is projective if and only if \(Q\) is primitive, i.e.
\[\gcd(A, B, C) = g(b^2 - ac, ad - bc, c^2 - bd) = 1\]
\end{proposition}

Once we've defined the concept of projectivity, we require the rather natural concept of reducibility:
\begin{definition}
\(f\) is \textbf{reducible} if it factors over \(\ints[x, y]\) (or equivalently over \(\rationals[x, y]\) by Gauss's lemma)
\end{definition}

Using the bijection between binary cubic forms and tuples \((O, I, \delta)\), we have the following lemma
\begin{lemma}
\((O, I, \delta)\) is reducible if and only if \(\delta = k^3\) is a cube.
\end{lemma}

The reducible forms hence form a subgroup of \(H(O)\), which we denote
\begin{definition}
\(H_{red}(O) = \{(O, I, \delta) : \delta = k^3\}\)
up to equivalence.
\end{definition}

Note that this forms a subgroup since the product of two cubes \(\delta = k^3, \delta' = k'^3\) is a cube \((kk')^3\).
\begin{definition}

The \(n\)-torsion subgroup \(G_n\) of a group \(G\) is the subgroup consisting of all elements with finite order \(n\), i.e.
\[G_n = \{x \in G : x^n = 1\}\]
\end{definition}

We can now define the last piece of notation used in the main result:
\begin{definition}
Let \(\mc{I}_3(O)\) denote the 3-torsion subgroup of the ideal group \(\mc{I}\) of \(\mc{O}\).
\end{definition}
We can now start to prove the main result. Recall the theorem
\begin{theorem}
Let \(\varphi: \mc{I}_3 \to H(O)\) be given by \(I \mapsto (O, I, 1)\). Then not only is \(\Im(\varphi) \subseteq H_{red}(\mc{O})\), but in fact \(\varphi\) maps \(\mc{I}_3\) isomorphically to it.
\end{theorem}
\begin{proposition}
\(\lim_{x \to \infty}\frac{1}{x}\sum_{0 < \Disc(O) < x}1 = \frac{1}{2}\)
\end{proposition}
\begin{proof}
Recall that
\[\mbb{D}(x) = \{z \in \ints^+ : z < x \quad \land \quad z \equiv 0, 1 \mod 4 \quad \land \quad z \text{ is non-square}\}\]
is in bijection with
\[\mbb{O}(x) = \{\text{all quadratic integral domains } O : 0 < \Disc(O) < X\}\]
Hence, we have that
\(\lim_{x \to \infty}\#\mbb{D}(x)/x = 1/2\), implying the desired result.
\end{proof}
\begin{proposition}
Let \(O\) be the quadratic ring with discriminant \(D\), and let \(h_{proj, red}(D)\) denote the number of \(\SL_2(\ints)\) equivalence classes of projective and reducible integer-matrix binary cubic forms of reduced discriminant \(D\). Then we have
\[\sum_{0 < D < x}|H_{red}(O)| = \sum_{0 < D < x}h_{proj, red}(D) = \frac{\zeta(2)}{2\zeta(3)} \cdot x + o(x)\]
\end{proposition}
We can now move on to proving the desired main result, which we restate:
\begin{theorem}
\[\lim_{x \to \infty}\frac{\sum_{0 < \Disc(O) < X}|I_3(O)|}{\sum_{0 < \Disc(O) < X}1} = \frac{\zeta(2)}{\zeta(3)}\]
\end{theorem}
Our first target is to show that
\[\sum_{0 < D < x}h_{proj, red}(D) = \frac{\zeta(2)}{2\zeta(3)} \cdot x + o(x)\]
\section*{Reduction Theory (section 4)}
Let \(f(x, y) = ax^3 + bx^2y + cxy^2 + dy^3\), with \(Q_f(x, y) = Ax^2 + Bx + Cy^2\). We have
\[\Disc(Q_f) = \smdisc(f) < 0\]
where \(Q_f\) is definite. We want to count the number of \(f\) such that \(Q_f\) is reduced. But let's first assume merely that
\[-A < B \leq A < C \quad \land \quad 0 < 4AC - B^2 < x\]
Since we have \(A = b^2 - ac\), \(B = ad - bc\), \(C = c^2 + bd\), we can get bounds on the values of \(a, b, c, d\). Specifically,
\begin{lemma}
\[|a| < \frac{\sqrt{2}}{\sqrt[4]{3}}x^{1/4}\]
\[|b| \leq \frac{\sqrt{2}}{\sqrt[4]{3}}x^{1/4}\]
\[|ad| \leq \frac{2}{\sqrt{3}}x^{1/2}\]
and other bounds.
\end{lemma}
\begin{proof}
\[Q(b, a) = Ca^2 + Bab + Ab^2 = A^2, \qquad Q(d, c) = Cd^2 + Bcd + Ad^2 = C^2\]
The rest is algebraic manipulation based off this trick, left as an exercise to the reader.
\end{proof}
\begin{corollary}
The number of integral binary cubic forms satisfying
\[-A < B \leq A \leq C \quad \land \quad 0 < 4AC - B^2 < x\]
such that \(A = C\) is \(\mc{O}(x^{3/4}\log x)\)
\end{corollary}
\begin{corollary}
The number of reducible binary cubic forms \(f(x, y) = ax^3 + bx^2y + cxy^2 + dy^3\) with \(a \neq 0\) satisfying
\[-A < B \leq A \leq C\] where \(0 < -\Disc(Q_f) < x\) is, for any \(\epsilon > 0\),
\[O(x^{3/4 + \epsilon})\]
\end{corollary}
So we don't need to worry about non-reduced forms, because they won't change our upper bound. These two lemmas can be found in Davenport as Lemmas 2 and 3.
We now, definining,
\[F(D) = \#\{\SL_2(\ints)\text{-onto binary cubic forms } f \text{ with } \smdisc(f) = D\}\]
\[h(D) = \#F(D), \qquad h'(D) = \{ax^3 + bx^2y + cxy^2 + dy^3 \in F(D) : a = 0, b > 0, Q_f \text{ reduced}\}\]
can conclude that that
\[\sum_{0 < -D < x}h(D) = \sum_{0 < -D < x}h'(D) + o(x^{3/4 + \epsilon})\]
As we have that
\[\sum_{0 < -D < x}h'(D) = \sum_{0 < b < \sqrt{2}x^{3/4}/\sqrt[4]{3}}\sum_{-b < c < b}\left(\frac{c^2}{b} - b\right) - \left(\frac{3c^2}{4b} - \frac{x}{4b^3}\right) + o(1)\]
(with the \(o(1)\) to ensure we remain within the integers). As there are only \(2b\) integers between \(-c\) and \(c\), we obtain
\[\sum_{0 < b \leq \sqrt{2}x^{3/4}/\sqrt[4]{3}}2b\frac{x}{4b^3} + O(b^2)
= x\left(\sum_{b \leq \sqrt{2}x^{3/4}/\sqrt[4]{3}}\frac{1}{b^2}\right) + O(b^2)\]
\[\leq x\sum_{b = \sqrt{2}x^{3/4}/\sqrt[4]{3}}\frac{1}{b^2} \leq x\int_{x^{1/4}}^\infty\frac{1}{b^2}db = \frac{\zeta(2)}{2} + o(x^{3/4})\]
And we'll end here since we're out of time. But the rest of the proof proceeds using the Mobius inverse functions and formulae, along with combinatoric methods.

\end{document}
