\documentclass{article}
\usepackage[utf8]{inputenc}

\title{Counting via Class Field Theory}
\author{Jad Elkhaleq Ghalayini}
\date{November 28 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}
\usepackage{braket}

\usepackage[margin=1in]{geometry}

\newtheorem*{theorem}{Theorem}
\newtheorem*{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}
\newcommand{\prt}[2]{{\frac{\partial #1}{\partial #2}}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\Span}{span}
\DeclareMathOperator{\Id}{id}
\DeclareMathOperator{\Image}{Image}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Disc}{Disc}
\DeclareMathOperator{\smdisc}{disc}
\DeclareMathOperator{\Pos}{Pos}
\DeclareMathOperator{\Gal}{Gal}
\DeclareMathOperator{\SL}{SL}
\DeclareMathOperator{\Cl}{Cl}

\newcommand{\TODO}[1]{\text{\textbf{TODO: }{#1}}}
\newcommand{\TODOD}[1]{\begin{center}\large{\TODO{#1}}\end{center}}

\begin{document}

\maketitle

\begin{center}
Based on a presentation by Tanny Libman
\end{center}

We're going to be doing some basic class field theory here. We'll begin by restating some definitions from earlier in the series of lectures, and then I will state and sketch a proof of a theorem by Davenport and Heilbraum. We will then look at the more general question of how the average size changes when we look at quadratic orders instead of maximal quadratic orders. So let's start with the definitions.
\begin{definition}
A \textbf{number field} \(K\) is a subfield of \(\comps\) which has finite degree over \(\rationals\).
\end{definition}
This is something we've seen before, immediately leading to the definition
\begin{definition}
Let \(\mc{O}_K\) denote the \textbf{ring of integers} of \(K\), i.e.
\[\mc{O}_K = \{\alpha \in K : \exists \text{monic} f \in \ints[x], f(x) = 0\}\]
\end{definition}
We have the basic properties that
\begin{proposition}
\(\mc{O}_K\) has \(K\) as its field of fractions.
\end{proposition}
\begin{proposition}
\(\mc{O}_k\) is a free \(\ints\)-module of rank \([K : \rationals]\)
\end{proposition}
So, this is all review so far. Now, fix a number field \(K\). The first thing we want to look at in the ring \(\mc{O}_K\) is how ideals factor. We have the following property:
\begin{proposition}
Any nonzero ideal \(a \subseteq \mc{O}_K\) can be written as a product \(a = p_1...p_n\) where \(p_i\) are the prime ideals containing \(a\) (which may be repeated). This factorization is unique up to ordering.
\end{proposition}
Note that this is a corollary of the fact that \(\mc{O}_K\) is a \textbf{Dedekind domain}, which also entails some other properties. We now proceed with the following definition
\begin{definition}
A \textbf{fractional ideal of} \(\mc{O}_K\) is a finitely-generated \(\mc{O}_K\)-submodule of \(K\). We denote the set of fractional ideals by \(I_K\). A \textbf{principal fractional ideal} is a set of the form \(\alpha\mc{O}_K\).
We denote the set of principal fractional ideals by \(P_K\).
\end{definition}
With these definitions in mind, we note that
\begin{proposition}
Fractional ideals are invertible, i.e. \(I_K\) is a group under ideal multiplication.
\end{proposition}
This allows us to define the class group as follows
\begin{definition}
We define the \textbf{class group} of \(K\) and \(\mc{O}_K\) to be given by
\[\Cl(K) = \Cl(\mc{O}_K) = I_K/P_K\]
\end{definition}
\begin{definition}
If \(K = \rationals(\sqrt{N})\) is a quadratic field (where \(N \neq 0, 1\) is squarefree) then the discriminant of \(K\) is given by
\[\Disc(K) = d_K = \left\{\begin{array}{cl}
  N & \text{if} \ N \equiv 1 \mod 4 \\
  4N & \text{otherwise}
\end{array}\right.\]
\end{definition}
We fix some notation:
\begin{definition}
For a quadratic field \(K_2\), let \(\Cl_3(K_2)\) denote the number of 3-torsion elements of \(\Cl(K_2)\)
\end{definition}
We can now state the result we want to prove:
\[
  \lim_{x \to \infty}\frac{\sum_{0 < d_{K_2} < x}\Cl_3(K_2)}{\sum_{0 < d_{K_2} < x}1} = \frac{4}{3}, \qquad
  \lim_{x \to \infty}\frac{\sum_{-x < d_{K_2} < 0}\Cl_3(K_2)}{\sum_{-x < d_{K_2} < 0}1} = 2
\]
We're not going to prove this quite yet, as we'll first begin exploring some class field theory. So again, fix a number field \(K\).
\begin{definition}
Let \(L \supset K\) be a finite field extension. If \(p\) is a prime ideal of \(\mc{O}_K\) then \(p\mc{O}_L\) is an ideal of \(\mc{O}_L\) and hence factorizes as
\[p\mc{O}_L = \beta_1^{e_1}...\beta_g^{e_g}\]
We say that \(p\) \textbf{ramifies} in \(L\) if \(e_i > 1\) for some \(i\). Otherwise, \(p\) is \textbf{unramified}.
\end{definition}
Based off this definition, we're going to say that essentially an extension is called unramified if all the primers are unramified. It's not quite a full definition, as we need to include these things called ``infinite primes" (whereas these prime ideals are called ``finite primes"). If all the finite primes are unramified, then we \textit{can} have an unramified field, but we need to check the infinite primes as well. But for our uses today, we'll call an extension in which all finite primes are unramified an unramified extension.
\begin{theorem}
Given a number field \(K\), there is a finite Galois extension \(L \supset K\) such that \(L\) is an unramified Abelian extenson of \(K\), and any other unramified Abelian extension of \(K\) lies in \(L\), i.e. it is a maximal unramified extension. \(L\) is called the \textbf{Hilbert class field} of \(K\).
\end{theorem}
A consequence of the fact that this thing exists is that now we can use some Galois theory to think about the class group. Specifically, we have a piece of the Artin Reciprocity theorem which tells us that
\begin{theorem}[Artin Reciprocity (piece)]
Given a number field \(K\),
\(\Cl(\mc{O}_K) \simeq \Gal(L/K)\)
\end{theorem}
\begin{corollary}
Given a number field \(K\), there is a one-to-one correspondence between unramified Abelian extensions of \(K\) and subgroups of \(\Cl(\mc{O}_K)\).
\end{corollary}
We can now sketch the proof of the theorem of Davenport-Heilbraum, which we restate below:
\begin{theorem}[Davenport-Heilbraum]
We have
\[
  \lim_{x \to \infty}\frac{\sum_{0 < d_{K_2} < x}\Cl_3(K_2)}{\sum_{0 < d_{K_2} < x}1} = \frac{4}{3}, \qquad
  \lim_{x \to \infty}\frac{\sum_{-x < d_{K_2} < 0}\Cl_3(K_2)}{\sum_{-x < d_{K_2} < 0}1} = 2
\]
where \(K_2\) ranges over quadratic number fields.
\end{theorem}
\begin{proof}
Let \(K_2\) be a quadratic field, and let \(K_6\) be an unramified Abelian cubic extension of \(K_2\). I'm going to ask you to believe that:
\begin{itemize}
  \item \(K_2/\rationals\) is Galois with Galois group \(S_3\).
  So, we know about the structure of \(S_3\): it has one index-2 subgroup and three isomorphic index-3 subgroups. And this means that \(K_6\) has \(K_2\) as a subfield and 3 isomorphic cubic subfields \(K_3, K_3', K_3''\). We have that

  \item Any cubic subfield of \(K_3 \subset K_6\) is \textbf{nowhere totally ramified}, i.e. at any prime \(p\), \(p\) does not factor as a maximal prime power in \(K_3\) (cube of a prime).
\end{itemize}
We can now keep going with the proof. The next step is to note this sort of converse relationship: any nowhere totally ramified cubic extension \(K_3\) corresponds to some \(K_2\) in this way. That is, given \(K_3\), if \(K_6\) is its Galois closure, and \(K_2 < K_6\) is the unique quadratic field in \(K_6\), then \(K_6/K_2\) is unramified precisely when \(K_3\) is nowhere totally ramified. So in other words, we really have a correspondence here.

Then, by the Artin reciprocity theorem, we have that the number of unramified Abelian cubic extensions \(K_6\) is exactly the number of index 3 subgroups of the class group \(\Cl(\mc{O}_K)\). Now, we're in the home stretch, believe it or not.

We've found a correspondence between index 3 subgroups of the class group and unramified Abelian extensions, but what we can say next is this relatively important general fact about finite Abelian groups, which is that for any prime \(p\), index \(p\) subgroups are in one-to-one correspondence with order \(p\) subgroups. As \(3\) is prime, every order 3 subgroup \(\{1, g, g^{-1}\}\) contains three 3-torsion elements (the identity, and two which you have to raise to the third power to get the identity).
Hence, we have that
\[\#\{\text{order 3 subgroups}\} = \frac{1}{2}\left(\Cl_2(K_2) - 1\right)\]
So, to recap, we have a correspondence between
\begin{itemize}
  \item Index 3 subgroups
  \item Unramified Abelian extensions
  \item Triples of isomorphic fields \(K_3\) which are nowhere totally ramified
\end{itemize}
But the latter have a correspondence with maximal isomorphism classes of cubic rings, which, as we've been talking about all semester, correspond to equivalence classes of binary cubic forms. So now, we're going to use something that we know about cubic forms, which we won't prove:
\begin{lemma}
Let \(\mc{V}\) denote the set of \(v \in V_{\ints}\) (integer binary cubic forms) corresponding to maximal cubic rings which are nowhere totally ramified. Note that we say that a ring is nowhere totally ramified if its corresponding field is. We say that
\[V_{\ints}^{(0)} = \{\text{positive discriminant forms}\}, \qquad V_{\ints}^{(1)} = \{\text{negative discriminant forms}\}\]
We have that
\[\lim_{x \to \infty}\frac{N(V \cap V_\ints^{(i)})}{x} = \frac{3}{n_i\pi^2}\]
where \(i \in \{0, 1\}\), where \(n_0 = 6\), \(n_1 = 2\) are the size of the automorphism groups.
\end{lemma}
We're going to need one more quick lemma, which is actually relatively elementary
\begin{lemma}
\[\lim_{x \to \infty}\frac{\sum_{0 < d_{K_2} < x}1}{x} = \lim_{x \to \infty}\frac{\sum_{-x < d_{K_2} < 0}1}{x} = \frac{3}{\pi^2}\]
\end{lemma}
This is actually not that hard to prove, but I won't. We do note, however, that the \(\pi^2\) comes from \(\zeta(2)\).

At this point, I'm going to skip the rest of the proof, as it is literally just algebraic manipulation. We basically plug
\[\#\{\text{order 3 subgroups}\} = \frac{1}{2}\left(\Cl_2(K_2) - 1\right)\]
into the correspondences that we have an apply our two lemmas to directly obtain the desired result.

\end{proof}
With the few minutes that I have left, I want to talk about the more general question about quadratic orders. So naturally, we have to start by defining them:
\begin{definition}
A \textbf{quadratic order} \(\mc{O}\) is any subring of containing \(1\) of a quadratic field \(K\), which is a free \(\ints\)-module of rank \(2\).
\end{definition}
An example of a quadratic order is the ring of integers \(\mc{O}_K\). In fact, \(\mc{O}_K\) is \underline{maximal} among quadratic orders inside \(K\), in other words, given any quadratic order \(\mc{O} \subset K\), we have \(\mc{O} \subset \mc{O}_K\). A quick lemma is
\begin{lemma}
Any quadratic order \(\mc{O} \subset K\) where \(\Disc(K) = d_K\) is of the form
\(\left\langle 1, fw_K \right\rangle\)
where
\[w_K = \frac{d_K + \sqrt{d_K}}{2}, \qquad f = [\mc{O}_K : \mc{O}]\]
The number \(f\) is called the \textbf{conductor} of \(\mc{O}\).
\end{lemma}
We can now define the discriminant of a quadratic order to be given by
\begin{definition}
\(\Disc(\mc{O}) = f^2d_k\)
\end{definition}
Note this is no longer squarefree, but it is nonsquare. An important lemma about this is
\begin{lemma}
\(\Disc(\mc{O})\) determines \(\mc{O}\) completely, and any nonsquare integer congruent to \(0\) or \(1\) mod 4 will occur as the discriminant of a quadratic order.
\end{lemma}
So, this is very similar to quadratic fields: that they are basically parametrized by this discriminant. One thing to note about quadratic orders is that they are generally \underline{not} Dedekind domains, i.e. we don't necessarily have unique factorization into prime ideals. But, what we can do in order to still talk about factorization of ideals, is to look at subsets of ideals. So we can still talk about fractional ideals as before, specifically,
\begin{definition}
A \textbf{fractional ideal of} \(\mc{O}\) is a finitely-generated \(\mc{O}\)-submodule of \(K\). A \textbf{principal fractional ideal} is a set of the form \(\alpha\mc{O}_K\).
\end{definition}
\begin{definition}
A fractional ideal \(b\) is called \textbf{proper} if
\[\mc{O} = \{\beta \in K : \beta b < b\}\]
(sometimes we call these projective).
\end{definition}
The important thing about proper ideals is that
\begin{proposition}
Proper ideals are invertible, and invertible ideals are proper
\end{proposition}
So we can define
\begin{definition}
Let \(I(\mc{O})\) denote the set of proper ideals, and let \(P(\mc{O})\) denote the set of principal fractional ideals. We have
\[\Cl(\mc{O}) = I(\mc{O})/P(\mc{O})\]
\end{definition}
We can now state the general result:
\begin{theorem}[Bhargava-Varma]
Let \(\Cl_3(\mc{O})\) denote the number of 3-torsion elements of \(\Cl(\mc{O})\). Then
\[\lim_{x \to \infty}
  \frac{\sum_{0 < \Disc(\mc{O}) < x}\Cl_3(\mc{O})}{\sum_{0 < \Disc(\mc{O}) < x}1}
  = 1 + \frac{\zeta(2)}{3\zeta(3)} > \frac{4}{3}, \qquad
\lim_{x \to \infty}
  \frac{\sum_{-x < \Disc(\mc{O}) < 0}\Cl_3(\mc{O})}{\sum_{-x < \Disc(\mc{O}) < 0}1}
  = 1 + \frac{\zeta(2)}{\zeta(3)} > 2\]
\end{theorem}
The proof of this is much more complicated, but involves the ``ray class group".

\end{document}
