\documentclass{article}
\usepackage[utf8]{inputenc}

\title{Density of Discriminants of Cubic Fields}
\author{Jad Elkhaleq Ghalayini}
\date{November 21 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}
\usepackage{braket}

\usepackage[margin=1in]{geometry}

\newtheorem*{theorem}{Theorem}
\newtheorem*{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}
\newcommand{\prt}[2]{{\frac{\partial #1}{\partial #2}}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\Span}{span}
\DeclareMathOperator{\Id}{id}
\DeclareMathOperator{\Image}{Image}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Disc}{Disc}
\DeclareMathOperator{\Pos}{Pos}
\DeclareMathOperator{\Gal}{Gal}
\DeclareMathOperator{\Ord}{ord}
\DeclareMathOperator{\Ct}{ct}

\newcommand{\TODO}[1]{\text{\textbf{TODO: }{#1}}}
\newcommand{\TODOD}[1]{\begin{center}\large{\TODO{#1}}\end{center}}

\begin{document}

\maketitle

\begin{center}
Based on a presentation and notes by Sina Zoghi
\end{center}

The main result we're going to attempt to prove today is Theorem 1 in the paper, which is as follows
\begin{theorem}
  Let \(N_3(\zeta, \eta)\) be the set of cubic fields (up to isomorphism) such that
  \(\zeta < \Disc(R) < \eta\).
  Then
  \[\lim_{X \to \infty}\frac{N_3(0, X)}{X} = \frac{1}{12\zeta(3)}\]
\end{theorem}
To get there, we'll have to work through 3 topics:
\begin{itemize}

  \item \(p\)-adic numbers, as we'll have to use \(p\)-adic densities

  \item Lemma 19, giving the density of certain cubic forms we'll be examining

  \item The uniformity estimate, which, unlike some of the preceding theorems, works with infinitely many congruence relations.

\end{itemize}
So, let's begin with \(p\)-adic numbers, since this will probably take the longest.

\section*{The field \(\rationals_p\) of \(p\)-adic numbers}

\begin{definition}
For each prime \(p\), the \(p\)-adic integers are defined to be formal power series
\[s = \sum_{i = 0}^\infty a_ip^i, \qquad \text{where} \ a_i \in \nats,  \ 0 \leq a_i \leq p \]
\end{definition}
We can identify \(s\) with the sequence of its coefficients \(s = (a_i) = (a_0, a_1,...)\)
writing
\[s \equiv a_0 \mod p, \qquad s \equiv a_1 \mod p^2, \qquad ... \qquad s \equiv a_n \mod p^n\]
Therefore, if we have two arbitrary sequences of integers \(s = (s_i), t = (t_i)\), the above induces an equivalence relationship
\[t \sim s \iff t_n \equiv s_n \mod p^{n + 1}\]
As a concrete example, in the 3-adic integers, we have
\[11 \equiv 2 \mod 3, 9 \implies 11 = (2, 2, 11, 11, ...)\]
Defining addition, multiplication and subtraction, and noting that \(\ints^{p^n}\) is always a field, we have that \(\ints^p\) is a ring with identity. Note that, for \(\alpha \in \ints_p\) if we can write
\[\alpha = (a_0, a_0 + a_1p, a_0 + a_1p + a_2p^2,...)\]
then \(p^n\) divides \(\alpha\) if and only if \(a_0 = ... = a_n = 0\). Hence, we can define a norm on \(\rationals_p\), the field of fractions of \(\ints_p\), as follows:
\begin{proposition}
If \(\alpha \in \ints_p\setminus\{0\}\) and \(\beta \in \ints_p\), then there exists a unique \(n \in \nats\) such that \(\alpha = p^n\beta\).
\end{proposition}
\begin{definition}
If \(\alpha \in \ints_p\), we define the \textbf{order} of \(\alpha\), \(\Ord_p(\alpha)\), to be \(\infty\) if \(a = 0\) or \(n\) if \(p^{-n}\alpha \in \ints_p\).
\end{definition}
In other words, \(\Ord_p(\alpha)\) is the highest power of \(p\) that divides \(\alpha\). We can now define
\begin{definition}
The \textbf{\(p\)-adic valuation} or \textbf{\(p\)-adic absolute value} of \(\alpha\), is given by
\[|0|_p = 0, \qquad |\alpha|_p = p^{-\Ord_p(\alpha)}\]
\end{definition}
We can use this to obtain a distance function \(d(\alpha, \beta) = |\alpha - \beta|_p\), making \(\ints_p\) into a metric space. We can hence talk about the convergence of \(p\)-adic numbers, justifying our previous power series notation, which shows that every element of \(\ints_p\) is a limit of an integer sequence (i.e. \(\ints\) is dense in \(\ints_p\), with \(\ints_p\) actually being the completion of \(\ints\) with respect to \(d\)). Topologically, \(\ints_p\) is compact.

If \(m, n \in \ints\setminus\{0\}\) such that \(p|mn\), we can let \(\Ord_p(p^km/n) = k\) and \(|p^km/n| = p - k\) to extend yield a definition for \(\Ord_p\) and a norm on \(\rationals\). We can hence use this to define \(\Ord_p\) and a norm on \(\rationals_p\) by setting \(\Ord_p(\alpha/\beta) = \Ord_p(\alpha) - \Ord_p(\beta)\), \(|\alpha/\beta|_p = |\alpha|_p|\beta|_p^{-1}\).
As a metric space \(\rationals_p\) is totally disconnected, i.e. every subset of \(\rationals_p\) is clopen. We can place a measure on it in many ways, one of which is:
\begin{definition}
We define the \textbf{counting measure} on \(\rationals_p\) for any compact open subset \(U \subseteq \rationals_p\) by, where \(i_U\) is the indicator function of \(U\),
\(\mu(U) = \sum_{x \in U}i_U(x)\)
\end{definition}
We've now developed the machinery we'll need.

\subsection*{Ramification}
Given a number field \(K\), i.e. a finite extension of the rationals (\([K : Q] \in \nats\)), let \(\mc{O}_K\) be an order of \(K\) and let \(P \subseteq \mc{O}_k\) be a prime ideal. What happens when \(P\) is lifted to \(\mc{O}_L\), i.e., what is \(P\mc{O}_L\), where \(L\) is an extension of \(K\). Since \(P\mc{O}_L\) can be factored into a product of primes, this question can be stated as ``will \(P\mc{O}_L\) remain prime?"

We have that \(P \cap \ints\) is a prime ideal of \(\ints\), generated by a prime \(p\) in \(\ints\). Since the residue field (the quotient of a ring by a maximal ideal) of \(p\ints\) is \(\ints/p\ints = \field{p}\), we, by analogy, are interested in the residue field \(\mc{O}_K/P\), which is a finite set containing \(\field{p}\). So, let's examine it:
\begin{definition}
The \textbf{inertial degree}, denoted \(f_p\), is the dimension of \(\mc{O}_k\) taken as an \(\field{p}\) vector space.
\end{definition}
It turns out we have \(f_p = [K : \rationals]\).
\begin{definition}
Let \(p\) be prime, and let \(P\) be the prime ideal of \(\mc{O}_k\) above \(p\). We call the exact power of \(p\) dividing \(p\ints\), written \(e_p\), the \textbf{ramification index}{F}
\end{definition}
\begin{definition}
Let \(p\) be a prime with factorization in \(\mc{O}_K\) given by \(p\mc{O}_k = P_1^{e_{P_1}}...P_g^{e_{P_g}}\). We say \(p\) is \textbf{ramified} if there is some \(e_{P_i} > 1\), otherwise, we say \(p\) is \textbf{unramified}. If \(g = n\), the degree of the extension, each \(P_i = P_1 = P\) and \(p\) is unramified (i.e. \(p\mc{O}_K = P^n\)), then we call \(p\) \textbf{totally unramified}.
\end{definition}
\begin{lemma}
\(n = [K : \rationals] = \sum_{i = 1}^ge_{P_i}f_{P_i}\)
\end{lemma}
There is a deep connection between ramification and the discriminant of the field \(\Delta_K\). In fact, if \(p\) is ramified then \(p\) and \(\Delta_K\) are associates, i.e. \(p | \Delta_K\) and \(\Delta_K | p\).

\section*{Lemma 19}

Let \(V_\ints\) denote the space of binary cubic forms over the integers \(\ints\), and define
\[\mc{U}_p = \{f \in V_{\ints} : f \text{ is maximal at } p\}, \qquad \mc{V}_p = \{f \in \mc{U}_p : f \ \text{is maximal at} \ p \  \text{and} \ p \ \text{is not totally ramified}\}\]
\begin{lemma}
We have
\(\mu(\mc{U}_p) = \frac{(p^3 - 1)(p^2 - 1)}{p^5}\), \(\mu(\mc{V}_p) = \frac{(p^2 - 1)^2}{p^4}\)
\end{lemma}
\begin{proof}
For the sake of time, we'll take this as granted.
\end{proof}

\section*{The Uniformity Estimate}

Let \(\mc{Z}_p = V_\ints - \mc{U}_p\). We can write \(\mc{Z}_p = \mc{W}_p \sqcup \mc{V}_p\) where
\[\mc{W}_p = \{f : f \text{ is not maximal at } p\}\]
\begin{proposition}
We can write
\[N_3(Z_p, x) \in \mc{O}(x^2/p)\]
\end{proposition}
\begin{proof}
We assume the following lemma:
\begin{lemma}
If \(R\) is prime at \(p\) then the number of subrings of \(R\) with index \(p\) is less than or equal to 3.
\end{lemma}
If \(R\) is maximal at \(p\) then by Lemma 13 \(R\) has a \(\ints\)-basis \(\langle 1, \omega, \theta \rangle\) such that either
\begin{enumerate}

  \item \(R = \ints + \ints(\omega/p) + \ints\theta\). In this case
  \(R'\) is primitive at \(p\) so
  \[\Disc(R') = \Disc(R)/p^2 < x/p^2\]

  \item \(R = \ints + \ints(\omega/p) + \ints(\theta/p)\). In this case
  \(\Disc(R'') < x/p^4\), which, by Theorem 6, means it is in \(O(x/p^2)\)

\end{enumerate}
So, \(N(W_p, x), N(V_p, x) \in O(x/p^2)\) giving the desired result.
\end{proof}
We now restate and prove the desired theorem
\begin{theorem}
\[\lim_{n \to \infty}\frac{N_3(0, x)}{x} = \frac{1}{12\zeta(3)}\]
\end{theorem}
\begin{proof}
Let \(U = \bigcap_pU_p\). We have
\[\mu(U_p) = (1 - p^{-2})(1 - p^{-3})\]
Let \(y \in \ints^+\). By Theorem 27
\[\lim_{x \to \infty}\frac{N(\bigcap_{p < y}U_p \cap V_\ints, z)}{x} = \frac{\pi^2}{12}\prod_{p < y}(1 - p^{-2})(1 - p^{-3})\]
Let \(y \to \infty\). Then
\[\lim\sup_{x \to \infty}\frac{N(U \cap V_\ints, x)}{x} \leq \frac{\pi^2}{12}[(1 - p^{-2})(1 - p^{-3})]\]
We have
\[\bigcap_{p < y}U_p \subset U \cup \bigcup_{p \geq y}W_p\]
Hence,
\[\lim_{x \to \infty}\frac{N(U \cap V_z), x}{x} = \frac{\pi^2}{12}\mu(U_p) = \frac{1}{12\zeta(3)}\]
And here's where the talk ends.
\end{proof}


\end{document}
